import sys
import os

""" CODE to generate one training file from a directory. 
	Arguments: 1. Name of the directory containing the training data
			   2. Name of the final aggregated output training file - spam_training.txt"""

rootdir = sys.argv[1]
outFileName = sys.argv[2]

outFile = open(outFileName, 'w')
    
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
#        outFile.write(file)
        fileName1, fileExtension1 = os.path.splitext(file)
        fileN11, fileN21 = os.path.splitext(fileName1)
        outFile.write(fileN11+" ")
        fn = os.path.join(rootdir, file)
        f = open(fn, 'r', errors='ignore')
        for line1 in f:
            outFile.write(line1.rstrip('\r\n'))
        outFile.write("\n")

outFile.close()