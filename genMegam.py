from collections import Counter, defaultdict
import re
import sys
import random
import math
import os
import operator
import pickle
import math

def tokenize(text):
    return re.findall('[A-Za-z0-9]+', text)

def readTrainingFile(fileName):
    vocab = defaultdict(int)
    with open(fileName,errors='ignore') as f:
        for line in f:
            label = line.split(' ',1)[0]
            for word in tokenize(line):
                vocab[word] += 1 
    vocabSize = len(vocab)
    return (vocab)
    
def main():
	count = 0
	trainingFile = sys.argv[1]
	modelFile = sys.argv[2]

	(vocab) = readTrainingFile(trainingFile)
	
	for key, value in vocab.items():
		print('vocab: ',key, value)		
		
	with open(modelFile, 'wb') as handle:
		pickle.dump(vocab, handle)
	

if __name__ == '__main__':
    main()