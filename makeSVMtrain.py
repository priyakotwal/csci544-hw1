from collections import Counter, defaultdict
import re
import sys
import random
import math
import os
import operator
import pickle

def tokenize(text):
    return re.findall('[A-Za-z0-9]+', text)
    
def classify(line, priors, likelihood, vocabSize,vocabDict,wordIndex , count):

    prob = Counter()
    priorsTotal = sum(priors.values())
    pword = 1.0
    
    #for c in vocabSize.keys():           
    for words in line:
        label = words.split(' ',1)[0]
      #  print('label ',label)
        if(label == 'HAM' or label == 'POS'):
            print('+1',end="")
        else:
            print('-1',end="")
        wordtoken = tokenize(words)
        lineMap = Counter()
        for word in wordtoken:
        	lineMap[wordIndex[word]] = vocabDict[wordIndex[word]][word]
        	#print(' ',wordIndex[word],': ',word,'=',vocabDict[wordIndex[word]][word],' ',end="")
        sorted_lineMap = sorted(lineMap.items(), key=operator.itemgetter(0))
        #print("\n",end="")
        for lineKeys, lineValues in sorted_lineMap:
        	print('',lineKeys,':',lineValues,end="")
       # print('map',sorted_lineMap)
        print("\n",end="")
            #p = p * ((likelihood[c][word] + 1 ) / (n + len(vocabSize)))
            #pword = pword + math.log((likelihood[c][word] + 1 ) / (n + len(vocabSize)))     
        # prob[c] = pword   

   # print(max(prob, key=prob.get))
  
    
def readTestingFile(fileName):
    return [line.strip().split('\n') for line in open(fileName,errors='ignore').readlines()]

def main():
	count = 0
	modelFile = sys.argv[1]
	testingFile = sys.argv[2]


	with open(modelFile, 'rb') as handle:
		priors, likelihood, vocab, vocabDict, wordIndex = pickle.load(handle)

	lines = readTestingFile(testingFile)

	for line in lines:
	    count = count + 1
	    classify(line, priors, likelihood, vocab, vocabDict, wordIndex, count)         
	
   
if __name__ == '__main__':
    main()