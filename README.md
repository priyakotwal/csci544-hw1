PART 1. NAIVE BAYES CLASSIFIER

		1. SPAM DATASET
	   
		Precision HAM:  0.9935483870967742
		Recall HAM:  0.9946178686759957
		F SCORE HAM:  0.9940828402366864
	
		Precision SPAM:  0.9946178686759957
		Recall SPAM:  0.9935483870967742
		F SCORE SPAM:  0.9940828402366864
	   
	   
		2. SENTIMENT DATASET

		Precision POS:  0.8424
		F SCORE POS:  0.8424000000000001
		Recall POS:  0.8424
		Precision NEG:  0.952
		Recall NEG:  0.8424
		F SCORE NEG:  0.952  
	   
	   
PART 2:  Using an off-the-shelf implementations of Maximum Entropy classification and Support Vector Machines

	   
1. Support Vector Machines
	
	1. SPAM DATASET
	   
	Precision HAM:  0.994
	Recall HAM:  0.994
	F SCORE HAM:  0.994
	
	Precision SPAM:  0.90633608815427
	Recall SPAM:  0.90633608815427
	F SCORE SPAM:  0.90633608815427
	   
	   
	2. SENTIMENT DATASET

	Precision POS:  0.9648
	F SCORE POS:  0.9648
	Recall POS:  0.9648
	
	Precision NEG:  0.972
	Recall NEG:  0.9648
	F SCORE NEG:  0.972  
	
	
2. Maximum Entropy classification
	
	1. SPAM DATASET
	   
	Precision HAM:  0.8237196765498652
	Recall HAM:  0.8280409041980624
	F SCORE HAM:  0.82587463791786
	
	Precision SPAM:  0.819059982094897
	Recall SPAM:  0.8195698924731183
	F SCORE SPAM:  0.81931485794682
	
	   
	2. SENTIMENT DATASET

	Precision POS:  0.639599555061179
	F SCORE POS:  0.6291028446389497
	Recall POS:  0.6189451022604952
	
	Precision NEG:  0.5783885071830106
	Recall POS:  0.6189451022604952
	F SCORE NEG:  0.7317265902805216 
	
PART 3:
Observation:  
- Accuracy is varied on a smaller dataset and not better since the training data will be small  
- The number of words to be learnt will be less which will result in many unknown words in the testing data  
- Hence the classification will be difficult and result in less recall.

Extra code files used:
1. calf1.py , calf1SENT.py: To calculate the precison , recall of spam and sentiment datasets respectively
2. genSVMspamTrain.py, makeSVMtrain.py, makeSVMtest.py: To generate the input files for SVM
3.  genMegam.py, makeMegamTest.py, makeMegam.py: To generate the input files for MegaM
4. convertSPAM.py: To convert the outputs of SVM and MEGAM to HAM/SPAM and POS/NEG
5. nbclassify.py, nblearn.py: NAIVE BAYES CLASSIFIER 

Commands to run the files used:



Pre-processing the training and testing files: 
	gentraining: python3 gentraining.py FOLDER_NAME/ training_output.txt 
	gentesting : python3  gentesting.py FOLDER_NAME/ testing_output.txt

Generating Labels from development data
	getLABELS: python3 getLABELS.py spam_dev_training.txt output.txt
	

PART 1 - NAIVE BAYES:

	nblearn : python3 nblearn.py training model
	nbclassify: python3 scripts/nbclassify.py model testing >out

Scripts to calculate the accuracy (development label file and out files)

	calf1 python3 scripts/calf1.py spam.dev.out spam_dev_labels.txt >spam_naive_accuracy.txt
	calf1SENT: python3 scripts/calf1SENT.py sentiment.dev.out sentiment_dev_labels.txt >sentiment_naive_accuracy.txt

PART 2 - SVM and MEGAM

Pre-processing files for svm:
Use genSVMspamTrain.py, makeSVMtrain.py, makeSVMtest.py: To generate the input files for SVM
remSpace python3 scripts/remSpace.py 

Commands to run SVM:
./svm_learn example1/train.dat example1/model
./svm_classify example1/test.dat example1/model example1/predictions

Post-processing SVM output:
python3 postprocessSPAM.py output spam.svm.out
python3 postprocessSENTIMENT.py output sentiment.svm.out

MEGAM
./megam -fvals -nc multiclass spam.megam.training.txt > spam.megam.model

./megam -fvals -predict spam.megam.model -nc multiclass spam.megam.testing.txt > spam.auto.megam.out




