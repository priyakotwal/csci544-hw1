from collections import Counter, defaultdict
import re
import sys
import random
import math
import os
import operator
import pickle

def tokenize(text):
    return re.findall('[A-Za-z0-9]+', text)
    
def classify(line, vocab):
    
    for words in line:
        label = words.split(' ',1)[0]
        words = words.split(' ',1)[1]
      #  print('label ',label)
        if(label == 'HAM' or label == 'POS'):
            print('1',end="")
        else:
            print('0',end="")
        wordtoken = tokenize(words)
        for word in wordtoken:
        	if word in vocab:
        		print('',word,vocab[word],end="")
        print("\n",end="")  
    
def readTestingFile(fileName):
    return [line.strip().split('\n') for line in open(fileName,errors='ignore').readlines()]

def main():
	modelFile = sys.argv[1]
	testingFile = sys.argv[2]

	with open(modelFile, 'rb') as handle:
		vocab = pickle.load(handle)

	lines = readTestingFile(testingFile)

	for line in lines:
	    classify(line, vocab)         
	
   
if __name__ == '__main__':
    main()