from collections import Counter, defaultdict
import re
import sys
import random
import math
import os
import operator
import pickle
import math

def tokenize(text):
    return re.findall('[A-Za-z0-9]+', text)

def readTrainingFile(fileName):
    priors = Counter()
    likelihood = defaultdict(Counter)
    vocab = defaultdict(int)
    with open(fileName,errors='ignore') as f:
        for line in f:
            label = line.split(' ',1)[0]
            priors[label] += 1
            """if(label == 'HAM' || label == 'POS'):
            	newLabel = '+1'
            	print('+1')
            else:
            	newLabel = '-1'
            	print('-1') """
            for word in tokenize(line):
                likelihood[label][word] += 1
                vocab[word] += 1 
    vocabSize = len(vocab)
    return (priors, likelihood , vocab)
    
def main():
	count = 0
	trainingFile = sys.argv[1]
	modelFile = sys.argv[2]
	vocabDict = defaultdict(Counter)
	wordIndex = defaultdict(int)

	(priors, likelihood, vocab) = readTrainingFile(trainingFile)
	#print(' ',vocab)
	
	i = 1
	for key, value in vocab.items():
		#print('vocab: ',key, value)		
		vocabDict[i][key] = math.log(float(value/len(vocab)))
		wordIndex[key] = i
		i = i+1
	#for index,word in vocabDict.items():
	#	print(index,' word:',word)
	#print('vocab: ', vocabDict)
	with open(modelFile, 'wb') as handle:
		pickle.dump((priors, likelihood, vocab, vocabDict,wordIndex), handle)
	

if __name__ == '__main__':
    main()