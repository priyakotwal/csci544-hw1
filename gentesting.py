import sys
import os

""" CODE to generate one testing file from a directory. 
	Arguments: 1. Name of the directory containing the testing data
			   2. Name of the final aggregated output testing file """

#rootdir = '/Users/priyakotwal/Documents/sharedUbuntu/544/test/'
rootdir = sys.argv[1]
outFileName = sys.argv[2]

outFile = open(outFileName, 'w')
    
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        fn1 = os.path.join(rootdir, file)
        f = open(fn1, 'r', errors='ignore')
        for line1 in f:
                outFile.write(line1.rstrip('\r\n'))
        outFile.write("\n")

outFile.close()