from collections import Counter
import sys

#arg1 correct output arg2 is obtained output
file1 = sys.argv[1]
file2 = sys.argv[2]
rf1 = open(file1)
rf2 = open(file2)
correct = 0
error = 0
total = 0
count1 = 0
count2 = 0
hamcount = 0
spamcount = 0
label1 = Counter()
label2 = Counter()
fscoreHAM = 0.0
fscoreSPAM  = 0.0
recallSPAM = 0.0
recallHAM = 0.0
precisionHAM =0.0
precisionSPAM =0.0
classHam = 0
classSpam = 0


for line1 in rf1:
    text1 = line1.rstrip().split(' ',1)[0]
    label1[count1] = text1
    count1 = count1 + 1

for line2 in rf2:
    text2 = line2.rstrip().split(' ',1)[0]
    label2[count2] = text2
    count2 = count2 + 1
print("l1 len",len(label1))
print("l2 len",len(label2))


for i in range(0,len(label1),1):
    print(i, 'label1 ',label1[i])
    print(i, 'label2 ',label2[i])
    if label2[i] == 'POS':
    	classHam = classHam + 1
    if label2[i] == 'NEG':
    	classSpam = classSpam + 1
    if label1[i] == label2[i] and label1[i]=='POS':
        hamcount = hamcount + 1
    elif label1[i] == label2[i] and label1[i]=='NEG':
        spamcount = spamcount + 1
    else:
    	error = error + 1
    total = total + 1

precisionHAM = float(hamcount/classHam)
recallHAM = float(hamcount/929)
precisionSPAM = float(spamcount/classSpam)
recallSPAM = float(spamcount/930)

fscoreHAM = float(2* ((precisionHAM*recallHAM)/(precisionHAM+recallHAM)))   
fscoreSPAM = float(2* ((precisionSPAM*recallSPAM)/(precisionSPAM+recallSPAM)))    
    
print('Precision POS: ', precisionHAM)
print('F SCORE POS: ', fscoreHAM)
print('Recall POS: ',recallHAM)
print('Precision NEG: ', precisionSPAM)
print('Recall POS: ',recallHAM)
print('F SCORE NEG: ', fscoreSPAM)


#print('correctly classified ',correct,'out of ',total,'err ',error)
#print('Accuracy: ',(correct/total)*100)
#fscore = float(2* ((precision*recall)/(precision+recall)))
#print('FSCORE: ',fscore)
  
 